import 'package:flutter/material.dart';

class CustomSnackbarWidget extends SnackBar {
  CustomSnackbarWidget({
    Key? key,
    required String message,
  }) : super(
          key: key,
          content: Text(message),
          duration: const Duration(seconds: 3),
        );
}
