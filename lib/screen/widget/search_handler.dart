import '../apiservices/getdata.dart';

Future<void> handleSearch(String apiUrl, String column, String value,
    Function(List<dynamic>) onDataChanged) async {
  var fullApiUrl = '$apiUrl?$column=$value';
  var data = await getData(fullApiUrl);

  // Simulate loading delay
  await Future.delayed(const Duration(seconds: 1));

  onDataChanged(data);
}
