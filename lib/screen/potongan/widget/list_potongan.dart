import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ListPotongan extends StatelessWidget {
  final Map<String, dynamic> potonganData;
  final bool isLastItem;

  const ListPotongan(
      {Key? key, required this.potonganData, required this.isLastItem})
      : super(key: key);

  String getInitial(String name) {
    return name.isNotEmpty ? name[0].toUpperCase() : '';
  }

  String formatCurrency(int amount) {
    final formatCurrency =
        NumberFormat.currency(locale: 'id_ID', symbol: 'Rp ');
    return formatCurrency.format(amount);
  }

  @override
  Widget build(BuildContext context) {
    final namaPotongan = potonganData['nama_pegawai'];
    final gaji = potonganData['gaji'];
    final formattedGaji = formatCurrency(gaji);
    final initial = getInitial(namaPotongan);

    return Container(
      padding: EdgeInsets.only(right: 8.0, bottom: isLastItem ? 8.0 : 0),
      child: Stack(
        children: [
          // Card with ListTile
          Padding(
            padding: const EdgeInsets.only(left: 50.0),
            child: Card(
              child: ListTile(
                title: Text(
                  namaPotongan,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Text('Gaji : $formattedGaji'),
              ),
            ),
          ),
          // Positioned CircleAvatar at the top-left corner
          Positioned(
            top: 4.0,
            left: 4.0,
            child: CircleAvatar(
              child: Text(initial),
            ),
          ),
        ],
      ),
    );
  }
}
