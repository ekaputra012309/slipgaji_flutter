// ignore_for_file: library_private_types_in_public_api, library_prefixes

import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart' as SearchBarLibrary;

class CustomSearchBar extends StatefulWidget implements PreferredSizeWidget {
  final String apiUrl;
  final String judul;
  final Function(String) onSubmitted;
  final Function() onClear;
  final Function(bool?) onFilterChanged;

  const CustomSearchBar({
    Key? key,
    required this.apiUrl,
    required this.judul,
    required this.onSubmitted,
    required this.onClear,
    required this.onFilterChanged,
  }) : super(key: key);

  @override
  _CustomSearchBarState createState() => _CustomSearchBarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _CustomSearchBarState extends State<CustomSearchBar> {
  late SearchBarLibrary.SearchBar searchBar;

  @override
  void initState() {
    super.initState();
    searchBar = SearchBarLibrary.SearchBar(
      inBar: true,
      setState: setState,
      onSubmitted: widget.onSubmitted,
      buildDefaultAppBar: _buildAppBar,
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      title: Text(widget.judul),
      centerTitle: true,
      actions: [
        if (widget.judul == 'Pegawai')
          IconButton(
            icon: const Icon(Icons.filter_list_rounded),
            onPressed: () => _showFilterOptions(context),
          ),
        searchBar.getSearchAction(context),
      ],
    );
  }

  void _showFilterOptions(BuildContext context) async {
    // Show modal bottom sheet with filter options
    bool? isActive = await showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Select Employee Status',
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.check_circle),
              title: const Text('Active Employee'),
              onTap: () {
                Navigator.pop(context, true);
              },
            ),
            ListTile(
              leading: const Icon(Icons.cancel),
              title: const Text('Non-Active Employee'),
              onTap: () {
                Navigator.pop(context, false);
              },
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: SizedBox(
                width: double.infinity, // Make the button full-width
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(
                        context, null); // Dismiss without selecting any option
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor:
                        Colors.green, // Set background color to green
                  ),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 12.0,
                    ),
                    child: Text(
                      'Reset',
                      style: TextStyle(
                          color: Colors.white), // Set text color to white
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );

    widget.onFilterChanged(isActive);
  }

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: widget.preferredSize,
      child: searchBar.build(context),
    );
  }
}
