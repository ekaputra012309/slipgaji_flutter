import 'package:flutter/material.dart';
import '../apiservices/getdata.dart';
import 'widget/list_potongan.dart';

class PotonganScreen extends StatefulWidget {
  const PotonganScreen({Key? key}) : super(key: key);

  @override
  State<PotonganScreen> createState() => _PotonganScreenState();
}

class _PotonganScreenState extends State<PotonganScreen> {
  var jsonList;
  bool isFabVisible = true;

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  Future<void> _fetchData() async {
    var apiUrl = '/api/potongan';
    var data = await getData(apiUrl);
    setState(() {
      jsonList = data;
    });
  }

  String getInitial(String name) {
    return name.isNotEmpty ? name[0].toUpperCase() : '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Potongan'),
        centerTitle: true,
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollUpdateNotification) {
            setState(() {
              // Use null-aware operator (??) to provide a default value if scrollDelta is null
              isFabVisible = (scrollNotification.scrollDelta ?? 0) <= 0;
            });
          }
          return true;
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 12.0,
          ),
          child: ListView.builder(
            itemCount: jsonList == null ? 0 : jsonList.length,
            itemBuilder: (BuildContext context, int i) {
              final isLastItem = i == jsonList.length - 1;
              return ListPotongan(
                  potonganData: jsonList[i], isLastItem: isLastItem);
            },
          ),
        ),
      ),
      floatingActionButton: isFabVisible
          ? FloatingActionButton(
              onPressed: () {
                // Add your FloatingActionButton onPressed logic here
              },
              child: const Icon(Icons.add),
            )
          : null, // Set to null when you want to hide the FloatingActionButton
    );
  }
}
