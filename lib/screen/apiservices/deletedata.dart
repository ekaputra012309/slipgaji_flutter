import 'package:dio/dio.dart';
import 'api_config.dart';

Future<String> deleteData(String apiUrl) async {
  try {
    var response = await Dio().delete(ApiConfig.baseUrl + apiUrl);

    if (response.statusCode == 204) {
      return 'Data deleted successfully';
    } else {
      print('Failed to delete data. Status code: ${response.statusCode}');
      return 'Failed to delete data. Status code: ${response.statusCode}';
    }
  } catch (e) {
    print('Error deleting data: $e');
    return 'Error deleting data: $e';
  }
}
