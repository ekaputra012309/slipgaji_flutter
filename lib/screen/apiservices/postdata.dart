import 'package:dio/dio.dart';
import 'api_config.dart';

Future<String> postData(
  String apiUrl,
  Map<String, dynamic> payload,
) async {
  try {
    var response = await Dio().post(
      ApiConfig.baseUrl + apiUrl,
      data: payload,
    );

    if (response.statusCode == 201) {
      return 'Data added successfully';
    } else {
      print('Failed to add data. Status code: ${response.statusCode}');
      return 'Failed to add data. Status code: ${response.statusCode}';
    }
  } catch (e) {
    print('Error adding data: $e');
    return 'Error adding data: $e';
  }
}
