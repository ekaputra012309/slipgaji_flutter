import 'package:flutter/material.dart';
// import '/screen/pegawai/pegawai_add.dart';
import 'screen/pegawai/pegawai_screen.dart';
// import 'screen/potongan/potongan_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Slip Gaji',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.green),
        useMaterial3: true,
      ),
      home: const PegawaiScreen(),
    );
  }
}
