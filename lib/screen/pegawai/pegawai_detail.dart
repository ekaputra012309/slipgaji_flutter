import 'package:flutter/material.dart';
import '../apiservices/getdatabyid.dart';

class PegawaiDetail extends StatefulWidget {
  final String id;

  const PegawaiDetail({Key? key, required this.id}) : super(key: key);

  @override
  State<PegawaiDetail> createState() => _PegawaiDetailState();
}

class _PegawaiDetailState extends State<PegawaiDetail> {
  late Map<String, dynamic> jsonData;

  @override
  void initState() {
    super.initState();
    jsonData = {};
    _fetchData();
  }

  Future<void> _fetchData() async {
    var apiUrl = '/api/pegawai/${widget.id}';
    var data = await getDataById(apiUrl);

    setState(() {
      jsonData = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail Pegawai'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new_rounded),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: ListView(
          children: [
            _buildDetailItem('NIP Pegawai', jsonData['nip'].toString()),
            _buildDetailItem(
                'Nama Pegawai', jsonData['nama_pegawai'].toString()),
            _buildDetailItem('Rekening Pegawai', jsonData['no_rek'].toString()),
            _buildDetailItem('Status Pegawai',
                jsonData['status'].toString() == '1' ? 'Aktif' : 'Non Aktif'),
          ],
        ),
      ),
    );
  }

  Widget _buildDetailItem(String title, String subtitle) {
    return Column(
      children: [
        ListTile(
          title: Text(title),
          subtitle: Text(subtitle),
        ),
        const Divider(),
      ],
    );
  }
}
