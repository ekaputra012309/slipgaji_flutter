// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import '../apiservices/getdatabyid.dart';
import '../apiservices/putdata.dart';
import '../widget/snackbar.dart';

class PegawaiEdit extends StatefulWidget {
  final String id;

  const PegawaiEdit({Key? key, required this.id}) : super(key: key);

  @override
  State<PegawaiEdit> createState() => _PegawaiEditState();
}

class _PegawaiEditState extends State<PegawaiEdit> {
  final _nip = TextEditingController();
  final _namaPegawai = TextEditingController();
  final _noRek = TextEditingController();
  late Map<String, dynamic> jsonData;

  @override
  void initState() {
    super.initState();
    jsonData = {};
    _fetchData();
  }

  Future<void> _fetchData() async {
    var apiUrl = '/api/pegawai/${widget.id}';
    var data = await getDataById(apiUrl);

    setState(() {
      jsonData = data;
      _nip.text = jsonData['nip'].toString();
      _namaPegawai.text = jsonData['nama_pegawai'].toString();
      _noRek.text = jsonData['no_rek'].toString();
    });
  }

  void _showCustomSnackbar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      CustomSnackbarWidget(message: message),
    );
  }

  Future<void> _updatedData(BuildContext context) async {
    var apiUrl = '/api/pegawai/${widget.id}';
    var updatedData = {
      'nip': _nip.text,
      'nama_pegawai': _namaPegawai.text,
      'no_rek': _noRek.text,
    };

    try {
      String resultMessage = await putData(apiUrl, updatedData);
      Navigator.pop(context, resultMessage);
    } catch (e) {
      print('Error adding data: $e');
      _showCustomSnackbar(context, 'Failed to add data. Please try again.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Pegawai'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new_rounded),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(
              right: 16.0,
            ),
            child: IconButton(
              onPressed: () {
                if (_validateInputs()) {
                  _updatedData(context);
                }
              },
              icon: const Icon(Icons.done),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(
          15,
        ),
        child: Column(
          children: [
            TextFormField(
              controller: _nip,
              keyboardType: TextInputType.number,
              readOnly: true,
              decoration: InputDecoration(
                labelText: 'NIP Pegawai', // Floating label
                hintText: 'Enter your NIP', // Initial hint
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            TextFormField(
              controller: _namaPegawai,
              maxLines: 2,
              decoration: InputDecoration(
                labelText: 'Nama Pegawai', // Floating label
                hintText: 'Enter your name', // Initial hint
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            TextFormField(
              controller: _noRek,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Rekening Pegawai', // Floating label
                hintText: 'Enter your account number', // Initial hint
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool _validateInputs() {
    List<String> errorMessages = [];

    if (_nip.text.isEmpty) {
      errorMessages.add('Please enter NIP');
    }

    if (_namaPegawai.text.isEmpty) {
      errorMessages.add('Please enter Nama Pegawai');
    }

    if (_noRek.text.isEmpty) {
      errorMessages.add('Please enter Rekening Pegawai');
    }

    if (errorMessages.isNotEmpty) {
      _showCustomSnackbar(context, errorMessages.join('\n'));
      return false;
    }

    return true;
  }
}
