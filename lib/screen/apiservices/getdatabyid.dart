import 'package:dio/dio.dart';
import 'api_config.dart';

Future<Map<String, dynamic>> getDataById(String apiUrl) async {
  try {
    var response = await Dio().get(ApiConfig.baseUrl + apiUrl);
    if (response.statusCode == 200) {
      return response.data as Map<String, dynamic>;
    } else {
      print(response.statusCode);
      return <String, dynamic>{}; // Return an empty map or handle the error
    }
  } catch (e) {
    print(e);
    return <String, dynamic>{}; // Return an empty map or handle the error
  }
}
