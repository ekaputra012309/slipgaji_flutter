import 'package:dio/dio.dart';
import 'api_config.dart';

Future<List> getData(String apiUrl) async {
  try {
    var response = await Dio().get(ApiConfig.baseUrl + apiUrl);
    if (response.statusCode == 200) {
      return response.data as List;
    } else {
      print(response.statusCode);
      return <dynamic>[]; // Return an empty list or handle the error
    }
  } catch (e) {
    print(e);
    return <dynamic>[]; // Return an empty list or handle the error
  }
}
