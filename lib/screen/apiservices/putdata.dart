import 'package:dio/dio.dart';
import 'api_config.dart';

Future<String> putData(
  String apiUrl,
  Map<String, dynamic> updatedData,
) async {
  try {
    var response = await Dio().put(
      ApiConfig.baseUrl + apiUrl,
      data: updatedData,
    );

    if (response.statusCode == 200) {
      return 'Data update successfully';
    } else {
      print('Failed to add data. Status code: ${response.statusCode}');
      return 'Failed to add data. Status code: ${response.statusCode}';
    }
  } catch (e) {
    print('Error adding data: $e');
    return 'Error adding data: $e';
  }
}
