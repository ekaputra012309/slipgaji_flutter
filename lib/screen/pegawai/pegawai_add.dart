// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';

import '../apiservices/postdata.dart';
import '../widget/snackbar.dart';

class PegawaiAdd extends StatefulWidget {
  const PegawaiAdd({super.key});

  @override
  State<PegawaiAdd> createState() => _PegawaiAddState();
}

class _PegawaiAddState extends State<PegawaiAdd> {
  final _nip = TextEditingController();
  final _namaPegawai = TextEditingController();
  final _noRek = TextEditingController();

  void _showCustomSnackbar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      CustomSnackbarWidget(message: message),
    );
  }

  Future<void> _addData(BuildContext context) async {
    var apiUrl = '/api/pegawai'; // Replace with your actual endpoint
    var payload = {
      'nip': _nip.text,
      'nama_pegawai': _namaPegawai.text,
      'no_rek': _noRek.text,
    };

    try {
      String resultMessage = await postData(apiUrl, payload);
      Navigator.pop(context, resultMessage);
    } catch (e) {
      print('Error adding data: $e');
      _showCustomSnackbar(context, 'Failed to add data. Please try again.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Pegawai'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new_rounded),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(
              right: 16.0,
            ),
            child: IconButton(
              onPressed: () {
                if (_validateInputs()) {
                  _addData(context);
                }
              },
              icon: const Icon(Icons.done),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(
          15,
        ),
        child: Column(
          children: [
            TextFormField(
              controller: _nip,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'NIP Pegawai', // Floating label
                hintText: 'Enter your NIP', // Initial hint
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            TextFormField(
              controller: _namaPegawai,
              maxLines: 2,
              decoration: InputDecoration(
                labelText: 'Nama Pegawai', // Floating label
                hintText: 'Enter your name', // Initial hint
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            TextFormField(
              controller: _noRek,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Rekening Pegawai', // Floating label
                hintText: 'Enter your account number', // Initial hint
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool _validateInputs() {
    List<String> errorMessages = [];

    if (_nip.text.isEmpty) {
      errorMessages.add('Please enter NIP');
    }

    if (_namaPegawai.text.isEmpty) {
      errorMessages.add('Please enter Nama Pegawai');
    }

    if (_noRek.text.isEmpty) {
      errorMessages.add('Please enter Rekening Pegawai');
    }

    if (errorMessages.isNotEmpty) {
      _showCustomSnackbar(context, errorMessages.join('\n'));
      return false;
    }

    return true;
  }
}
