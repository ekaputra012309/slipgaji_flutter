// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import '../widget/custom_search_bar.dart';
import '../widget/loading.dart';
import '../widget/search_handler.dart';
import '../widget/snackbar.dart';
import '/screen/pegawai/pegawai_add.dart';
import '../apiservices/getdata.dart';
import 'widget/list_pegawai.dart';

class PegawaiScreen extends StatefulWidget {
  const PegawaiScreen({Key? key}) : super(key: key);

  @override
  State<PegawaiScreen> createState() => _PegawaiScreenState();
}

class _PegawaiScreenState extends State<PegawaiScreen> {
  late List<dynamic> jsonList;
  bool isFabVisible = true;
  bool isRefreshing = false;
  bool filterIsActive = false;
  String defaultApi = '/api/pegawai';

  @override
  void initState() {
    super.initState();
    jsonList = [];
    _fetchData(defaultApi);
  }

  Future<void> _fetchData(String apiUrl) async {
    var data = await getData(apiUrl);
    setState(() {
      jsonList = data;
    });
  }

  void _handleSearch(String value) {
    handleSearch(defaultApi, 'nama_pegawai', value, (List<dynamic> data) {
      setState(() {
        jsonList = data;
        isRefreshing = false;
      });
    });

    setState(() {
      isRefreshing = true;
    });
  }

  void _handleClear() {
    // Handle clearing search, if needed
    // You can put logic to reset the list to the original state, for example
    _fetchData(defaultApi);
  }

  Future<void> _handleRefresh(String urlApi) async {
    setState(() {
      isRefreshing = true; // Set to true when the refresh starts
    });

    // Simulate loading delay
    await Future.delayed(const Duration(seconds: 1));

    await _fetchData(urlApi);

    setState(() {
      isRefreshing = false; // Set back to false when the refresh completes
    });
  }

  Future<void> _handleRefreshWOurl() async {
    setState(() {
      isRefreshing = true; // Set to true when the refresh starts
    });

    // Simulate loading delay
    await Future.delayed(const Duration(seconds: 1));

    await _fetchData(defaultApi);

    setState(() {
      isRefreshing = false; // Set back to false when the refresh completes
    });
  }

  void _handleFilterChanged(bool? isActive) {
    setState(() {
      filterIsActive = isActive ?? false;
    });

    // Refresh data only if isActive is not null, otherwise, fetch default data
    if (isActive != null) {
      String apiurl =
          isActive == true ? '$defaultApi?status=1' : '$defaultApi?status=0';
      _handleRefresh(apiurl);
    }
    if (isActive == null) {
      // Fetch default data when isActive is null
      _handleRefresh(defaultApi);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomSearchBar(
        apiUrl: defaultApi, // Adjust the API endpoint
        judul: 'Pegawai',
        onSubmitted: _handleSearch,
        onClear: _handleClear,
        onFilterChanged: _handleFilterChanged,
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollUpdateNotification) {
            setState(() {
              isFabVisible = (scrollNotification.scrollDelta ?? 0) <= 0;
            });
          }
          return true;
        },
        child: RefreshIndicator(
          onRefresh: _handleRefreshWOurl,
          child: CustomScrollView(
            slivers: <Widget>[
              // Show loading animation while refreshing
              if (isRefreshing)
                const SliverFillRemaining(
                  child: LoadingAnimation(),
                )
              else
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int i) {
                      bool isLastItem = i == jsonList.length - 1;
                      return Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 8.0,
                          vertical: isLastItem ? 8.0 : 0,
                        ),
                        child: Column(
                          children: [
                            ListPegawai(
                              jsonList[i],
                              onRefresh: _handleRefreshWOurl,
                            ),
                            if (!isLastItem) const Divider(),
                          ],
                        ),
                      );
                    },
                    childCount: jsonList.length,
                  ),
                ),
            ],
          ),
        ),
      ),
      floatingActionButton: isFabVisible
          ? FloatingActionButton(
              onPressed: () async {
                var result = await Navigator.push<String>(
                  context,
                  MaterialPageRoute(builder: (context) => const PegawaiAdd()),
                );

                if (result != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    CustomSnackbarWidget(message: result),
                  );
                  _handleRefresh(defaultApi);
                }
              },
              child: const Icon(Icons.add),
            )
          : null,
    );
  }
}
