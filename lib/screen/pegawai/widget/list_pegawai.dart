// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';

import '../../apiservices/deletedata.dart';
import '../../apiservices/putdata.dart';
import '../../widget/snackbar.dart';
import '../pegawai_detail.dart';
import '../pegawai_edit.dart';

class ListPegawai extends StatefulWidget {
  final Map<String, dynamic> pegawaiData;
  final RefreshCallback onRefresh;

  const ListPegawai(this.pegawaiData, {required this.onRefresh, Key? key})
      : super(key: key);

  @override
  State<ListPegawai> createState() => _ListPegawaiState();
}

class _ListPegawaiState extends State<ListPegawai> {
  late bool isToggleOn;

  @override
  void initState() {
    super.initState();
    // Set the initial state of the toggle based on pegawaiData['status']
    isToggleOn = widget.pegawaiData['status'] == '1';
  }

  String getInitial(String name) {
    return name.isNotEmpty ? name[0].toUpperCase() : '';
  }

  @override
  Widget build(BuildContext context) {
    final namaPegawai = widget.pegawaiData['nama_pegawai'];
    final idPegawai = widget.pegawaiData['id'];
    final initial = getInitial(namaPegawai);

    return GestureDetector(
      onTap: () {
        // Show a bottom sheet with options for edit and delete
        showModalBottomSheet<void>(
          context: context,
          builder: (BuildContext context) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ListTile(
                  leading: const Icon(Icons.info_outline_rounded),
                  title: const Text('Detail'),
                  onTap: () {
                    Navigator.pop(context); // Dismiss the bottom sheet
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            PegawaiDetail(id: idPegawai.toString()),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: const Icon(Icons.edit),
                  title: const Text('Edit'),
                  onTap: () async {
                    var result = await Navigator.push<String>(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              PegawaiEdit(id: idPegawai.toString())),
                    );
                    Navigator.pop(context);
                    if (result != null) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        CustomSnackbarWidget(message: result),
                      );

                      // Call the callback function to trigger data refresh
                      widget.onRefresh();
                    }
                  },
                ),
                ListTile(
                  leading: const Icon(Icons.delete),
                  title: const Text('Delete'),
                  onTap: () async {
                    // Show a confirmation bottom sheet for delete
                    bool deleteConfirmed = await showModalBottomSheet<bool>(
                          context: context,
                          builder: (BuildContext context) {
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const ListTile(
                                  leading: Icon(Icons.delete),
                                  title: Text('Delete Data'),
                                  subtitle: Text(
                                      'Anda yakin ingin menghapus data ini ?'),
                                ),
                                ListTile(
                                  leading: const Icon(Icons.check),
                                  title: const Text('Ya, Hapus'),
                                  onTap: () {
                                    Navigator.of(context).pop(true);
                                  },
                                ),
                                ListTile(
                                  leading: const Icon(Icons.cancel),
                                  title: const Text('Batal'),
                                  onTap: () {
                                    Navigator.of(context).pop(false);
                                  },
                                ),
                              ],
                            );
                          },
                        ) ??
                        false; // Provide a default value (false) if showModalBottomSheet returns null

                    // If user confirmed deletion, proceed with the deleteData API call
                    if (deleteConfirmed) {
                      String deleteMessage =
                          await deleteData('/api/pegawai/$idPegawai');
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(deleteMessage),
                        ),
                      );

                      // Call the callback function to trigger data refresh
                      widget.onRefresh();
                    }
                    Navigator.pop(context); // Close the initial bottom sheet
                  },
                ),
              ],
            );
          },
        );
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          children: [
            // Row for the name and avatar
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CircleAvatar(
                  child: Text(initial),
                ),
                const SizedBox(width: 8.0),
                Expanded(
                  child: ListTile(
                    title: Text(
                      namaPegawai,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Switch(
                  value: isToggleOn,
                  onChanged: (bool newValue) async {
                    // Show a confirmation bottom sheet based on the new toggle state
                    bool confirmation = (await showModalBottomSheet<bool>(
                          context: context,
                          builder: (BuildContext context) {
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                ListTile(
                                  leading: const Icon(Icons.question_answer),
                                  title: Text(newValue
                                      ? 'Activate Pegawai'
                                      : 'Non-Activate Pegawai'),
                                  subtitle: Text('Are you sure you want to '
                                      '${newValue ? 'activate' : 'non-activate'} Pegawai?'),
                                ),
                                ListTile(
                                  leading: const Icon(Icons.check),
                                  title: const Text('Yes'),
                                  onTap: () {
                                    Navigator.of(context).pop(true);
                                  },
                                ),
                                ListTile(
                                  leading: const Icon(Icons.cancel),
                                  title: const Text('No'),
                                  onTap: () {
                                    Navigator.of(context).pop(false);
                                  },
                                ),
                              ],
                            );
                          },
                        )) ??
                        false;

                    // If user confirmed the action, update the toggle state and perform action
                    if (confirmation) {
                      setState(() {
                        isToggleOn = newValue;
                      });

                      try {
                        String status = newValue ? '1' : '0';
                        Map<String, dynamic> updatedData = {
                          'status': status,
                        };
                        // print(updatedData);
                        String updateMessage = await putData(
                          '/api/pegawai/$idPegawai',
                          updatedData,
                        );

                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(updateMessage),
                          ),
                        );

                        // Call the callback function to trigger data refresh
                        widget
                            .onRefresh(); // You might want to rename this function accordingly
                      } catch (error) {
                        // Handle error
                        print('Error updating data: $error');
                      }
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
